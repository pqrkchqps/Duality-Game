// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

#include "EnergyBall.h"
#include "GameFramework/Actor.h"
#include "Math/RandomStream.h"
#include "UObject/ConstructorHelpers.h"
#include <string>



struct EnergyBallException : public std::exception
{
	std::string s;
	EnergyBallException(std::string ss) : s(ss) {}
	~EnergyBallException() throw () {} // Updated
	const char* what() const throw() { return s.c_str(); }
};

// Sets default values
AEnergyBall::AEnergyBall(const FObjectInitializer& ObjectInitializer)
{
	//AEnergyBall::Super(ObjectInitializer);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshOb_AW2(TEXT("StaticMesh'/Game/sphere.sphere'"));
	Mesh = StaticMeshOb_AW2.Object;
	MeshControl = ObjectInitializer.CreateDefaultSubobject < UStaticMeshComponent >(this, TEXT("MeshControl"));
	MeshControl->SetStaticMesh(Mesh);
	RootComponent = MeshControl;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	randomStream = FRandomStream();

	MaterialBlue = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Blue.Material_Energy_Ball_Blue'")).Object;
	MaterialGreen = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Green.Material_Energy_Ball_Green'")).Object;
	MaterialRed = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Red.Material_Energy_Ball_Red'")).Object;
	MaterialYellow = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Yellow.Material_Energy_Ball_Yellow'")).Object;
	MaterialWhite = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_White.Material_Energy_Ball_White'")).Object;
	MaterialViolet = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Violet.Material_Energy_Ball_Violet'")).Object;
	MaterialBlueSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Blue_Selected.Material_Energy_Ball_Blue_Selected'")).Object;
	MaterialGreenSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Green_Selected.Material_Energy_Ball_Green_Selected'")).Object;
	MaterialRedSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Red_Selected.Material_Energy_Ball_Red_Selected'")).Object;
	MaterialYellowSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Yellow_Selected.Material_Energy_Ball_Yellow_Selected'")).Object;
	MaterialWhiteSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_White_Selected.Material_Energy_Ball_White_Selected'")).Object;
	MaterialVioletSelected = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("Material'/Game/Material_Energy_Ball_Violet_Selected.Material_Energy_Ball_Violet_Selected'")).Object;
}

void AEnergyBall::SetColor(EColor::Color color) {
	switch (color) {
	case EColor::Color::Blue: MeshControl->SetMaterial(0, MaterialBlue); break;
	case EColor::Color::Green: MeshControl->SetMaterial(0, MaterialGreen); break;
	case EColor::Color::Red: MeshControl->SetMaterial(0, MaterialRed); break;
	case EColor::Color::Yellow: MeshControl->SetMaterial(0, MaterialYellow); break;
	case EColor::Color::White: MeshControl->SetMaterial(0, MaterialWhite); break;
	case EColor::Color::Violet: MeshControl->SetMaterial(0, MaterialViolet); break;
	case EColor::Color::BlueSelected: MeshControl->SetMaterial(0, MaterialBlueSelected); break;
	case EColor::Color::GreenSelected: MeshControl->SetMaterial(0, MaterialGreenSelected); break;
	case EColor::Color::RedSelected: MeshControl->SetMaterial(0, MaterialRedSelected); break;
	case EColor::Color::YellowSelected: MeshControl->SetMaterial(0, MaterialYellowSelected); break;
	case EColor::Color::WhiteSelected: MeshControl->SetMaterial(0, MaterialWhiteSelected); break;
	case EColor::Color::VioletSelected: MeshControl->SetMaterial(0, MaterialVioletSelected); break;
	default: throw EnergyBallException("SetColor Argument Is Not EColor Value");
	}
}

// Called when the game starts or when spawned
void AEnergyBall::BeginPlay()
{
	Super::BeginPlay();
}


bool AEnergyBall::ShouldTickIfViewportsOnly() const
{
	return true;
}

// Called every frame
void AEnergyBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	int32 rand_rotation1 = randomStream.RandRange(-7200, 7200);
	int32 rand_rotation2= randomStream.RandRange(-7200, 7200);
	int32 rand_rotation3 = randomStream.RandRange(-7200, 7200);
	FRotator DeltaRotation = FRotator(rand_rotation1 * DeltaTime, rand_rotation2 *DeltaTime, rand_rotation3 *DeltaTime);
	AddActorLocalRotation(DeltaRotation);
}

