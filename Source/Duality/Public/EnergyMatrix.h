// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

#pragma once
#include "Duality.h"
#include "GameFramework/Actor.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "EnergyBall.h"
#include "EnergyMatrix.generated.h"

#define CUBE_SIZE 2
#define CAMERA_ZOOM_START 490
#define CAMERA_ZOOM_STEP 330
#define CUBE_EXTENT 288
#define CUBE_PIECE_TAG "EnergyBall" 


UCLASS()
class DUALITY_API AEnergyMatrix : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnergyMatrix();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void BuildCube();

	UPROPERTY(Category = Duality, EditAnywhere)
	TSubclassOf<AEnergyBall> PieceClass;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USceneComponent * DummyRoot;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USpringArmComponent * SpringArm;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	USceneComponent * PieceRotator;

	UPROPERTY(Category = Duality, VisibleAnywhere)
	UCameraComponent * Camera;
};
