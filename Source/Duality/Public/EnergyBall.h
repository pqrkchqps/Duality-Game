// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

#pragma once

#include "Duality.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInterface.h"
#include "EColor.h"
#include "EnergyBall.generated.h"

UCLASS()
class DUALITY_API AEnergyBall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnergyBall(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual bool ShouldTickIfViewportsOnly() const override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AEnergyBall::SetColor(EColor::Color color);

private:
	UPROPERTY()
	UMaterialInterface *MaterialBlue;
	UPROPERTY()
	UMaterialInterface *MaterialGreen;
	UPROPERTY()
	UMaterialInterface *MaterialRed;
	UPROPERTY()
	UMaterialInterface *MaterialYellow;
	UPROPERTY()
	UMaterialInterface *MaterialWhite;
	UPROPERTY()
	UMaterialInterface *MaterialViolet;
	UPROPERTY()
	UMaterialInterface *MaterialBlueSelected;
	UPROPERTY()
	UMaterialInterface *MaterialGreenSelected;
	UPROPERTY()
	UMaterialInterface *MaterialRedSelected;
	UPROPERTY()
	UMaterialInterface *MaterialYellowSelected;
	UPROPERTY()
	UMaterialInterface *MaterialWhiteSelected;
	UPROPERTY()
	UMaterialInterface *MaterialVioletSelected;

	FRandomStream randomStream;

	UPROPERTY()
	UStaticMesh * Mesh;

	UPROPERTY()
	UStaticMeshComponent * MeshControl;
};
