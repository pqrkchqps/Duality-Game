// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

using UnrealBuildTool;
using System.Collections.Generic;

public class DualityTarget : TargetRules
{
	public DualityTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Duality" } );
	}
}
