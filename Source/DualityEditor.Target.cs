// @2017 Dreamer Assist Corporation ALL RIGHTS RESERVED

using UnrealBuildTool;
using System.Collections.Generic;

public class DualityEditorTarget : TargetRules
{
	public DualityEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Duality" } );
	}
}
